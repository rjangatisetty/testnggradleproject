package tests;

import applicationmodels.AppModel1;
import com.hp.lft.report.ReportException;
import com.hp.lft.report.Reporter;
import com.hp.lft.sdk.GeneralLeanFtException;
import com.hp.lft.sdk.ModifiableSDKConfiguration;
import com.hp.lft.sdk.SDK;
import com.hp.lft.sdk.SDKConfigurationFactory;
import framework.LeanFTConfig;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class Test001 extends LeanFTConfig{

    @Test(groups = {"regression"})
    public void Test001 () {

        try {
            new ProcessBuilder("C:\\Windows\\System32\\notepad.exe").start();
            getAppModel().notepadWindow().editEditor().sendKeys("Hello");
            System.out.println("Script is Execute Successfully");
        } catch (GeneralLeanFtException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

}

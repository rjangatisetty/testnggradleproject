package framework;

import applicationmodels.AppModel1;
import com.hp.lft.report.Reporter;
import com.hp.lft.sdk.ModifiableSDKConfiguration;
import com.hp.lft.sdk.SDK;
import com.hp.lft.sdk.SDKConfigurationFactory;
import com.hp.lft.sdk.web.Browser;
import com.hp.lft.sdk.web.BrowserFactory;
import com.hp.lft.sdk.web.BrowserType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class LeanFTConfig {


    @BeforeMethod(alwaysRun=true)
    public void configuration() {
        AppModel1 appModel1;
        try {
            ModifiableSDKConfiguration config = new ModifiableSDKConfiguration();
            config =  SDKConfigurationFactory.loadConfigurationFromExternalPropertiesFile("src\\main\\resources\\leanft.properties");
            SDK.init(config);
            Reporter.init();
            //browser = BrowserFactory.launch(BrowserType.CHROME);
            appModel1 = new AppModel1();
            Long threadID = Thread.currentThread().getId();
            CommonDataStorage.modelMap.put(threadID, appModel1);
        }catch (Exception ex){
            System.out.println("Exception occured " + ex.toString());
        }
    }

    public AppModel1 getAppModel () {
        Long key = Thread.currentThread().getId();
        return CommonDataStorage.modelMap.get(key);
    }
}
